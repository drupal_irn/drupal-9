# ddev-drupal9
*Drupal 9 DDEV based development container for Visual Studio Code with Remote - Containers.*

Provides a plug and play DDEV (Docker) based development environment for VSCode IDE with predefined webks GmbH best practice
- Extensions
- Settings
- Launch configuration (Run & Debug)
beautifully packaged for easy project and environment switching.

## Prerequisites:
  1. Up to date Version of ddev, Docker, Chrome/Firefox
  2. VSCode installed on your machine locally
  3. The "Remote Development" Extension in VSCode (extension name: ms-vscode-remote.vscode-remote-extensionpack)

## How to Use it:
 1. Download the the repo to a new empty project directory: `git clone git@gitlab.com:drupal_irn/drupal-9.git`
 2. Change into the directory: `cd drupal-9`
 3. Use `ddev start` to start up the environment
 4. Use `composer i` to install dependancies
 5. Use `ddev import-db --file=./data/database.sql` to import the database
 6. Use `ddev launch` to launch the app from the chrome
 7. You are ready to go! Use `ddev describe` to check the status of your Project!